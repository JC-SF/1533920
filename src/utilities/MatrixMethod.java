package utilities;

public class MatrixMethod {
	
	public static int[][] duplicate(int[][] duplicate) {
		int k = duplicate[0].length;
		int[][] newArray = new int[duplicate.length][k*2]; 
		for(int i = 0; i<duplicate.length; i++) {
			for(int j = 0; j<k; j++) {
				newArray[i][j] = duplicate[i][j];
			}
			for(int j = k; j<k*2; j++) {
				newArray[i][j] = duplicate[i][j-k];
			}
		}
		return newArray;
	}
	
}
