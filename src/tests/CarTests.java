package tests;
import vehicles.Car;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CarTests {

	@Test
	void testCarConstructor() {
		//This portion should not throw an error. 
		//Even though Cars should be initialized with 0 as the speed.
		Car myCar = new Car(10);
		Car myCar2 = new Car(56);
		Car myCar3 = new Car(0);
		//This portion should throw and error and should be caught.
		//If not the test fails.
		try { 
			Car myCarNegative = new Car(-1);
			//At this point of the test, the program should catch the block.
			//If not the test fails.
			fail("The test should've jumped to catch block.");
		}
		catch(IllegalArgumentException e) {
			
		}
	}
	
	@Test
	void testGetSpeed() {
		//Create Car Objects with different initial speed to test getSpeed method.
		Car myCar = new Car(0);
		Car myCar2 = new Car(45);
		Car myCar3 = new Car(1);
		Car myCar4 = new Car(10023);
		assertEquals(0, myCar.getSpeed());
		assertEquals(45, myCar2.getSpeed());
		assertEquals(1, myCar3.getSpeed());
		assertEquals(10023, myCar4.getSpeed());
	}
	
	@Test
	/*
		Create Car Objects with different initial speed to test getLocation method.
		Since I am only testing the location, I will not use decelerate or accelerate.
		The decelerate and accelerate method will be used in other tests. I am only testing the
		location method. If any error occurs, it is potentially from either MoveRight() or 
		MoveLeft(), or getLocation() method.
	*/
	void testGetLocation() {
		//Test for positive location.
		Car myCar = new Car(0);
		assertEquals(0, myCar.getLocation());
		myCar = new Car(89);
		myCar.moveRight();
		assertEquals(89, myCar.getLocation());
		myCar.moveRight();
		assertEquals(178, myCar.getLocation());
		//Test for negative location.
		myCar = new Car(34);
		myCar.moveLeft();
		assertEquals(-34, myCar.getLocation());
		myCar.moveLeft();
		assertEquals(-68, myCar.getLocation());
	}
	/*
	 * The accelerate method is tested before moveRight test because
	 * we want to make sure that moveRight is not simply the following equation:
	 * this.location = speed*(multipleOfMoveRightCall). 
	 */
	@Test
	void testAccelerate() {
		Car myCar = new Car(0);
		for(int i = 0; i<54; i++) {
			myCar.accelerate();
		}
		assertEquals(54, myCar.getSpeed());
	} 
	
	void testDecelerate() {
		Car myCar = new Car(0);
		for(int i = 0; i<54; i++) {
			myCar.accelerate();
		}
		for(int i = 54; i>13; i--) {
			myCar.decelerate();
		}
		assertEquals(12, myCar.getSpeed());
	}
	@Test
	void testMoveRight() {
		Car myCar = new Car(67);
		myCar.moveRight();
		assertEquals(67, myCar.getLocation());
		myCar.moveRight();
		assertEquals(134, myCar.getLocation());
		for(int i = 67; i>17; i--) {
			myCar.decelerate();
		}
		myCar.moveRight();
		assertEquals(151, myCar.getLocation());
	}
	
	@Test
	void testMoveLeft() {
		Car myCar = new Car(92);
		myCar.moveRight();
		assertEquals(92, myCar.getLocation());
		myCar.moveRight();
		assertEquals(184, myCar.getLocation());
		for(int i = 92; i>34; i--) {
			myCar.decelerate();
		}
		myCar.moveLeft();
		assertEquals(150, myCar.getLocation());
	}
	
	
}
