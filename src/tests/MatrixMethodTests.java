package tests;
import static org.junit.jupiter.api.Assertions.*;
import utilities.MatrixMethod;

import org.junit.jupiter.api.Test;

class MatrixMethodTests {

	/*
	 * This method tests duplicate method of MatrixMethod.duplicate().
	 */
	@Test
	void testMatrixDuplicateMethod() {
		int[][] myArray = {{2,3,4},{7,8,9}};
		int[][] expectedArray = {{2,3,4,2,3,4},{7,8,9,7,8,9}};
		assertArrayEquals(expectedArray, MatrixMethod.duplicate(myArray));
		
		int[][] myArray2 = {{2,5,6,9,8},{4,5,6,9,0},{9,8,5,4,6},{1,2,3,4,5}};
		int[][] expectedArray2 = {{2,5,6,9,8,2,5,6,9,8},
				{4,5,6,9,0,4,5,6,9,0},
				{9,8,5,4,6,9,8,5,4,6},
				{1,2,3,4,5,1,2,3,4,5}};
		assertArrayEquals(expectedArray2, MatrixMethod.duplicate(myArray2));
	}

}
